import React, {useEffect, useState} from "react"



function PresentationForm() {

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [companyName, setCompanyName] = useState('')
  const [title, setTitle] = useState('')
  const [synopsis, setSynopsis] = useState('')
  const [conference, setConference] = useState('')
  const [conferences, setConferences] = useState([])

  const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);

  const handleNameChange = (event) => {
    const value = event.target.value
    setName(value)
  }

  const handleEmailChange = (event) => {
    const value = event.target.value
    setEmail(value)
  }

  const handleCompanyChange = (event) => {
    const value = event.target.value
    setCompanyName(value)
  }

  const handleTitleChange = (event) => {
    const value = event.target.value
    setTitle(value)
  }

  const handleSynopsisChange = (event) => {
    const value = event.target.value
    setSynopsis(value)
  }

  const handleConferenceChange = (event) => {
    const value = event.target.value
    setConference(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()

    const data = {}

    data.name = name
    data.email =email
    data.company_name= companyName
    data.title= title
    data.synopsis = synopsis
    data.conference = conference


    console.log(data)

    const presentationUrl = 'http://localhost:8000/api/presentations/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json'
      }
    }

    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();

      setName('')
      setEmail('')
      setCompanyName('')
      setTitle('')
      setSynopsis('')
      setConference('')
  }
  }
    return(
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit}id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label for="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange}placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label for="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyChange}placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label for="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange}placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label for="title">Title</label>
              </div>
              <div className="mb-3">
                <label for="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange}class="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
              </div>
              <div className="mb-3">
              <select onChange={handleConferenceChange} name="conference" id="conference" className="form-control" required>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )
                    })}
                  </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
}

export default PresentationForm
